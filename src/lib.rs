pub fn builder() -> Builder {
    Gitmod::builder()
}

#[derive(Debug)]
pub struct Gitmod {
    url: String,
    target: Target,
    path: std::path::PathBuf,
    rmdir_on_failed: bool,
    submodule: Submodule,
}

impl Gitmod {
    pub fn builder() -> Builder {
        Default::default()
    }

    /// Clone the repository to the specified directory.
    ///
    /// Ignored, if it has already been cloned.
    pub fn init(self) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        func::init(
            self.url,
            self.target,
            self.path,
            self.rmdir_on_failed,
            self.submodule,
        )?;
        Ok(())
    }
}

#[derive(Debug, Default, Clone)]
pub struct Builder {
    url: Option<String>,
    target: Option<Target>,
    path: Option<std::path::PathBuf>,
    rmdir_on_failed: bool,
    submodule: Submodule,
}

impl Builder {
    /// repository url
    pub fn url(mut self, value: impl Into<String>) -> Self {
        self.url.replace(value.into());
        self
    }

    /// checkout target
    pub fn target(mut self, value: Target) -> Self {
        self.target.replace(value);
        self
    }

    /// repository clone destination
    pub fn path(mut self, value: impl AsRef<std::path::Path>) -> Self {
        self.path.replace(value.as_ref().to_path_buf());
        self
    }

    /// true, delete the specified dir on failure
    ///
    /// default: false
    pub fn rmdir_on_failed(mut self, value: bool) -> Self {
        self.rmdir_on_failed = value;
        self
    }

    /// Submodule resolution
    ///
    /// default: [Submodule::None]
    pub fn submodule(mut self, value: Submodule) -> Self {
        self.submodule = value;
        self
    }

    pub fn build(self) -> Gitmod {
        Gitmod {
            url: self.url.expect("url is not set"),
            target: self.target.expect("target is not set"),
            path: self.path.expect("path is not set"),
            rmdir_on_failed: self.rmdir_on_failed,
            submodule: self.submodule,
        }
    }
}

/// Checkout target
#[derive(Debug, Clone)]
pub struct Target {
    inner: TargetInner,
}

#[derive(Debug, Clone)]
enum TargetInner {
    Tag(String),
    Branch(String),
    Commit(String),
}

impl Target {
    /// Specify a name.
    ///
    /// Note: Searches are performed by backward matching, not by full name.
    pub fn tag(name: impl Into<String>) -> Self {
        Self {
            inner: TargetInner::Tag(name.into()),
        }
    }

    /// Specify a full name.
    pub fn branch(name: impl Into<String>) -> Self {
        Self {
            inner: TargetInner::Branch(name.into()),
        }
    }

    /// Specify a full SHA.
    pub fn commit(refname: impl Into<String>) -> Self {
        Self {
            inner: TargetInner::Commit(refname.into()),
        }
    }
}

/// Submodule resolution
#[derive(Debug, Clone, Default)]
pub enum Submodule {
    /// Do nothing
    #[default]
    None,
    /// Fetch all
    Init,
    /// Fetch all recursively
    InitRecursive,
}

mod repo {
    use git2::AutotagOption;

    pub(super) fn builder<'cb>() -> git2::build::RepoBuilder<'cb> {
        let mut builder = git2::build::RepoBuilder::new();
        builder.fetch_options(fetch_opts());
        builder
    }

    pub(super) fn fetch_opts<'cb>() -> git2::FetchOptions<'cb> {
        let mut callbacks = git2::RemoteCallbacks::new();
        callbacks.credentials(git_credentials_callback);

        let mut opts = git2::FetchOptions::new();
        opts.download_tags(AutotagOption::All);
        opts.remote_callbacks(callbacks);
        opts
    }

    fn git_credentials_callback(
        user: &str,
        user_from_url: Option<&str>,
        cred: git2::CredentialType,
    ) -> Result<git2::Cred, git2::Error> {
        let user = user_from_url.unwrap_or(user);
        if cred.contains(git2::CredentialType::USERNAME) {
            return git2::Cred::username(user);
        }

        #[cfg(feature = "ssh")]
        {
            git2::Cred::ssh_key_from_agent(user)
        }

        #[cfg(not(feature = "ssh"))]
        {
            Err(git2::Error::from_str("failed get credentials"))
        }
    }
}

mod func {
    use std::str;

    use git2::{AutotagOption, ObjectType, Repository, ResetType};

    use crate::repo::fetch_opts;

    use super::*;

    pub(super) fn init(
        url: String,
        target: Target,
        dir_path: std::path::PathBuf,
        rmdir_on_failed: bool,
        submodule: Submodule,
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        if let Err(e) = init_inner(url, target, &dir_path, submodule) {
            if rmdir_on_failed {
                let _ = std::fs::remove_dir_all(&dir_path);
            }
            return Err(e);
        }
        Ok(())
    }

    fn init_inner(
        url: String,
        target: Target,
        dir_path: &std::path::Path,
        submodule: Submodule,
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        let repo = if !dir_path.exists() {
            std::fs::create_dir_all(dir_path)?;
            repo::builder().clone(&url, dir_path)?
        } else {
            Repository::open(dir_path)?
        };

        let is_differ = {
            let origin = repo.find_remote("origin")?;
            origin.url().unwrap_or_default() != url
        };

        let repo = if is_differ {
            std::fs::remove_dir_all(dir_path)?;
            repo::builder().clone(&url, dir_path)?
        } else {
            repo
        };

        fetch(&repo)?;

        let oid = match target.inner {
            TargetInner::Tag(x) => {
                let mut opt_oid: Option<git2::Oid> = None;
                repo.tag_foreach(|oid, name| {
                    let tag = String::from_utf8(name.to_vec());
                    let Ok(tag) = tag else {
                        return true;
                    };
                    if tag.ends_with(&x) {
                        opt_oid.replace(oid);
                        false
                    } else {
                        true
                    }
                })?;
                let oid = opt_oid.ok_or(git2::Error::from_str("dont resolve tag oid"))?;
                repo.set_head_detached(oid)?;
                oid
            }
            TargetInner::Branch(x) => {
                let name = format!("origin/{x}");
                let b = repo.find_branch(&name, git2::BranchType::Remote)?;
                let oid = b
                    .get()
                    .target()
                    .ok_or(git2::Error::from_str("dont resolve tag oid"))?;
                repo.set_head_detached(oid)?;
                oid
            }
            TargetInner::Commit(x) => {
                let oid = git2::Oid::from_str(&x)?;
                repo.set_head_detached(oid)?;
                oid
            }
        };

        let obj = repo.find_object(oid, Some(ObjectType::Any))?;
        repo.reset(&obj, ResetType::Hard, None)?;

        match submodule {
            Submodule::None => return Ok(()),
            Submodule::Init => init_submodule(&repo, false)?,
            Submodule::InitRecursive => init_submodule(&repo, true)?,
        }
        Ok(())
    }

    fn init_submodule(
        repo: &Repository,
        recursive: bool,
    ) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
        for mut sm in repo.submodules()? {
            let mut opts = git2::SubmoduleUpdateOptions::new();
            opts.allow_fetch(true);
            opts.fetch(fetch_opts());
            sm.update(true, Some(&mut opts))?;
            if !recursive {
                return Ok(());
            }
            init_submodule(&sm.open()?, recursive)?;
        }
        Ok(())
    }

    fn fetch(repo: &Repository) -> Result<(), git2::Error> {
        let mut remote = repo
            .find_remote("origin")
            .or_else(|_| repo.remote_anonymous("origin"))?;
        remote.download(&[] as &[&str], Some(&mut fetch_opts()))?;
        {
            let stats = remote.stats();
            if stats.local_objects() > 0 {
                println!(
                    "\rReceived {}/{} objects in {} bytes (used {} local \
                 objects)",
                    stats.indexed_objects(),
                    stats.total_objects(),
                    stats.received_bytes(),
                    stats.local_objects()
                );
            } else {
                println!(
                    "\rReceived {}/{} objects in {} bytes",
                    stats.indexed_objects(),
                    stats.total_objects(),
                    stats.received_bytes()
                );
            }
        }
        remote.disconnect()?;
        remote.update_tips(None, true, AutotagOption::Unspecified, None)?;
        Ok(())
    }
}
