# Introduction

This crate is for resolving external repositories at build time without resorting to `git submodules`.

# Usage

**Cargo.toml**

```toml
[build-dependencies]
gitmod = { git = "https://gitlab.com/KetcKtsD/gitmod.git", tag = "0.1.0", features = ["ssh"] }
```

**build.rs**

```rust
use std::env::current_dir;

use gitmod::{Submodule, Target};

fn main() {
    let path = current_dir().unwrap().join(".repo");
    let result = gitmod::builder()
        .url("git@gitlab.com:KetcKtsD/ringbuf-rs.git")
        .target(Target::tag("0.1.0"))
        .path(path)
        .rmdir_on_failed(false)
        .submodule(Submodule::None)
        .build()
        .init();

    if let Err(e) = result {
        println!("{e}");
    }
}
```
