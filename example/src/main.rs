use std::env::current_dir;

use gitmod::{Submodule, Target};

fn main() {
    let path = current_dir().unwrap().join(".repo");
    let result = gitmod::builder()
        .url("git@gitlab.com:KetcKtsD/ringbuf-rs.git")
        .target(Target::tag("0.1.0"))
        .path(path)
        .rmdir_on_failed(false)
        .submodule(Submodule::None)
        .build()
        .init();

    if let Err(e) = result {
        println!("{e}");
    }
}
